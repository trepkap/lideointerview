package org.ptrepka.lideo.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Set;

@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor
@Entity
public class Customer {

    @Id
    @GeneratedValue
    @ApiModelProperty(hidden = true)
    private Long id;

    @NotBlank
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @ApiModelProperty(hidden = true)
    private Set<Address> addresses;
}
