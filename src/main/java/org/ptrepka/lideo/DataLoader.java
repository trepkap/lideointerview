package org.ptrepka.lideo;

import com.google.common.collect.ImmutableSet;
import org.ptrepka.lideo.boundary.AddressRepositoryRest;
import org.ptrepka.lideo.boundary.CustomerRepositoryRest;
import org.ptrepka.lideo.entity.Address;
import org.ptrepka.lideo.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private CustomerRepositoryRest customerRepositoryRest;

    @Autowired
    private AddressRepositoryRest addressRepositoryRest;

    public void run(ApplicationArguments args) {
        customerRepositoryRest.save(Customer.of(null, "First customer", ImmutableSet.of(Address.of(null, "Wrocław", "Główna", "1A", "12345"))));
        customerRepositoryRest.save(Customer.of(null, "Second customer", Collections.emptySet()));

        addressRepositoryRest.save(Address.of(null, "Wrocław", "Krakowska", "45", "98765"));
    }

}