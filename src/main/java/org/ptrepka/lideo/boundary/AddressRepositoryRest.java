package org.ptrepka.lideo.boundary;

import io.swagger.annotations.Api;
import org.ptrepka.lideo.entity.Address;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Api(tags = "Address Entity")
@RepositoryRestResource(path = "addresses")
public interface AddressRepositoryRest extends PagingAndSortingRepository<Address, Long> {
}
