package org.ptrepka.lideo.boundary;

import io.swagger.annotations.Api;
import org.ptrepka.lideo.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@Api(tags = "Customer Entity")
@RepositoryRestResource(path = "customers")
public interface CustomerRepositoryRest extends PagingAndSortingRepository<Customer, Long> {
}
