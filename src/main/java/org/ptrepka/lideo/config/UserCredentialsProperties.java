package org.ptrepka.lideo.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("credential.user")
@Getter
@Setter
public class UserCredentialsProperties {

    private String name;
    private String password;
}
