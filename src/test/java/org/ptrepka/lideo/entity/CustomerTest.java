package org.ptrepka.lideo.entity;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Collections;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerTest {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeClass
    public static void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void shouldCreateObjectWithoutViolation() {
        //given
        final Customer customer = Customer.of(
                1l,
                "Customer Name",
                Collections.emptySet()
        );

        //when
        final Set<ConstraintViolation<Customer>> violations
                = validator.validate(customer);

        //then:
        assertThat(violations).isEmpty();
    }

    @Test
    public void shouldEmptyNameCauseViolation() {
        //given
        final Customer customer = Customer.of(
                1l,
                "",
                Collections.emptySet()
        );

        //when
        final Set<ConstraintViolation<Customer>> violations
                = validator.validate(customer);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldNullNameCauseViolation() {
        //given
        final Customer customer = Customer.of(
                1l,
                null,
                Collections.emptySet()
        );

        //when
        final Set<ConstraintViolation<Customer>> violations
                = validator.validate(customer);

        //then:
        assertThat(violations).hasSize(1);
    }
}
