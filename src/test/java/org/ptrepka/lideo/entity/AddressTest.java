package org.ptrepka.lideo.entity;

import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class AddressTest {

    private static ValidatorFactory validatorFactory;
    private static Validator validator;

    @BeforeClass
    public static void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @Test
    public void shouldCreateObjectWithoutViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                "4A",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).isEmpty();
    }

    @Test
    public void shouldEmptyCityCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "",
                "Street",
                "4A",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldNullCityCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                null,
                "Street",
                "4A",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldEmptyStreetCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "",
                "4A",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldNullStreetCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                null,
                "4A",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldEmptyStreetNumberCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                "",
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldNullStreetNumberCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                null,
                "12345"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldEmptyPostalCodeNumberCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                "4a",
                ""
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(2);
    }

    @Test
    public void shouldNullPostalCodeCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                "4A",
                null
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }

    @Test
    public void shouldPostalCodeStreetNumberCauseViolation() {
        //given
        final Address address = Address.of(
                1l,
                "City",
                "Street",
                "4A",
                "123456"
        );

        //when
        final Set<ConstraintViolation<Address>> violations
                = validator.validate(address);

        //then:
        assertThat(violations).hasSize(1);
    }
}
