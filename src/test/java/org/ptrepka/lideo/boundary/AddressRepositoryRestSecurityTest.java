package org.ptrepka.lideo.boundary;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ptrepka.lideo.entity.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.ptrepka.lideo.utils.JsonUtils.asJsonString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AddressRepositoryRestSecurityTest {

    private static final String ADDRESSES_ENDPOINT = "/addresses";

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAddressesUnauthenticatedUser() throws Exception {
        mvc.perform(get(ADDRESSES_ENDPOINT))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void getAddressesAnonymousUser() throws Exception {
        mvc.perform(get(ADDRESSES_ENDPOINT))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void getAddressesAuthenticatedUser() throws Exception {
        mvc.perform(get(ADDRESSES_ENDPOINT))
                .andExpect(status().isOk());
    }

    @Test
    public void createAddressUnauthenticatedUser() throws Exception {
        mvc.perform(post(ADDRESSES_ENDPOINT).with(csrf()).content(asJsonString(createAddress())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void createAddressAnonymousUser() throws Exception {
        mvc.perform(post(ADDRESSES_ENDPOINT).with(csrf()).content(asJsonString(createAddress())))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void createCustomerAuthenticatedUser() throws Exception {
        mvc.perform(post(ADDRESSES_ENDPOINT).with(csrf()).content(asJsonString(createAddress())))
                .andExpect(status().isCreated());
    }

    private Address createAddress() {
        return Address.of(
                1l,
                "City",
                "Street",
                "4A",
                "12345"
        );
    }


}