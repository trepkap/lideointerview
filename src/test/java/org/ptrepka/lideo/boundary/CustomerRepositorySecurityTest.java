package org.ptrepka.lideo.boundary;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ptrepka.lideo.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.ptrepka.lideo.utils.JsonUtils.asJsonString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CustomerRepositorySecurityTest {

    private static final String CUSTOMERS_ENDPOINT = "/customers";

    @Autowired
    private MockMvc mvc;

    @Test
    public void getCustomersUnauthenticatedUser() throws Exception {
        mvc.perform(get(CUSTOMERS_ENDPOINT))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void getCustomersAnonymousUser() throws Exception {
        mvc.perform(get(CUSTOMERS_ENDPOINT))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void getCustomersAuthenticatedUser() throws Exception {
        mvc.perform(get(CUSTOMERS_ENDPOINT))
                .andExpect(status().isOk());
    }

    @Test
    public void createCustomerUnauthenticatedUser() throws Exception {
        mvc.perform(post(CUSTOMERS_ENDPOINT).with(csrf()).content(asJsonString(Customer.of(null, "Name", Collections.emptySet()))))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithAnonymousUser
    public void createCustomerAnonymousUser() throws Exception {
        mvc.perform(post(CUSTOMERS_ENDPOINT).with(csrf()).content(asJsonString(Customer.of(null, "Name", Collections.emptySet()))))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void createCustomerAuthenticatedUser() throws Exception {
        mvc.perform(post(CUSTOMERS_ENDPOINT).with(csrf()).content(asJsonString(Customer.of(null, "Name", Collections.emptySet()))))
                .andExpect(status().isCreated());
    }
}
