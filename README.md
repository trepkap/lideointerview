#Lideo Interview Application

To login into application please use credentials:
* login `admin`, pass: `admin`

Lideo Interview exposes swagger:
* `/swagger-ui.html`


## Build and run
* `./gradlew clean buld` - build without creating docker image
* `./gradlew test` - runs test only
* `./gradlew buildDocker` - create docker image
* `docker run -p 8080:8080 <docker-image-id>` - runs docker image

## Example

To create new customer send the POST request to `/customers`

```
{
    "name": "Customer name" 
}
```

If you want to add an address to particular customer, firstly the address has to be created.

To create the address send the POST request to `/addresses`

```
{
    "city": "City name",
    "postalCode": "12345",
    "street": "Street name",
    "streetNumber": "1A"
}

```

Then you can connect the customer with the address send the POST request base on a resource to `/customers/{id}/addresses`.

For the address resource location `http://localhost:8080/addresses/1` the request body is:

```
http://localhost:8080/addresses/1
```

If you want to check all customers send the GET request to `/customers`

If you want to check all addresses per customer send the GET request to `/customers/{id}/addresses`

If you want to check all addresses send the GET request to `/addresses`
